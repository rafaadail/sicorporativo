/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment';
import { Form, HasError, AlertError } from 'vform';

import swal from 'sweetalert2'
window.swal = swal;

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

window.toast = toast;

window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import VueRouter from 'vue-router'
Vue.use(VueRouter)
import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
  })



let routes = [
    { path: '/dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/developer', component: require('./components/Developer.vue').default },
    { path: '/usuarios', component: require('./components/Usuarios.vue').default },
    { path: '/perfil', component: require('./components/Perfil.vue').default },
    { path: '/cargos', component: require('./components/Cargos.vue').default },
    { path: '/centrocusto', component: require('./components/CentroCusto.vue').default },
    { path: '/centrotrabalho', component: require('./components/CentroTrabalho.vue').default },
    { path: '/categoria', component: require('./components/Categoria.vue').default },
    { path: '/produto', component: require('./components/Produto.vue').default },
    { path: '/cliente', component: require('./components/Cliente.vue').default },
    { path: '/plataforma', component: require('./components/Plataforma.vue').default },
    { path: '/fabricante', component: require('./components/Fabricante.vue').default },
    { path: '/formapagamento', component: require('./components/FormaPagamento').default },
    { path: '/statuspagamento', component: require('./components/StatusPagamento.vue').default },
    { path: '/statusvenda', component: require('./components/StatusVenda.vue').default },
    { path: '/pdv', component: require('./components/Pdv.vue').default },
    { path: '/preco', component: require('./components/ConsultarPreco.vue').default },
    { path: '/pedidovenda', component: require('./components/PedidoVenda.vue').default }
    // { path: '/select2', component: require('./components/Select2.vue').default }
]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})

Vue.filter('upText', function(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
});


Vue.filter('myDate', function(created) {
    return moment(created).format('D/MM/YYYY')});

Vue.filter('theDate', function(dateString) {
    return moment(dateString).format('YYYY-MM-DD')});

Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL',
        minimumFractionDigits: 2
    });
    return formatter.format(value);
});

window.Fire =  new Vue();

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component('grafica-component', require('./components/graficos.vue').default);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
