<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Starter</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
            </button>
            </div>
        </div>
    </form>

    <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="./img/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Loja</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="./img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">
              {{ Auth::user()->name }}
          </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

            <li class="nav-item">
            <router-link to="/dashboard" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </router-link>
            </li>
            <li class="nav-item">
                <router-link to="/pdv" class="nav-link">
                    <i class="nav-icon fas fa-cash-register"></i>
                    <p>
                        PDV
                    </p>
                </router-link>
                </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="nav-icon fa fa-cog"></i>
                  <p>
                    Cadastros
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <router-link to="/categoria" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Categoria</p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/cliente" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Cliente</p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/fabricante" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Fabricante</p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/formapagamento" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Forma de Pagamento</p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/pedidovenda" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Pedido Venda</p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/plataforma" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Plataforma</p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/statuspagamento" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Status Pagamento</p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/statusvenda" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Status Venda</p>
                        </router-link>
                    </li>
                    {{-- <li class="nav-item">
                        <router-link to="/centrocusto" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Centro de Custo</p>
                        </router-link>
                    </li> --}}
                    {{-- <li class="nav-item">
                        <router-link to="/centrotrabalho" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Centro de Trabalho</p>
                        </router-link>
                    </li> --}}
                    <li class="nav-item">
                        <router-link to="/produto" class="nav-link">
                            <i class="fa fa-circle nav-icon"></i>
                            <p>Produto</p>
                        </router-link>
                    </li>
                </ul>
              </li>
              <li class="nav-item">
                <router-link to="/preco" class="nav-link">
                    <i class="nav-icon fa fa-barcode"></i>
                    <p>
                        Preço Produto
                    </p>
                </router-link>
                </li>
            <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Sistema
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <router-link to="/cargos" class="nav-link">
                        <i class="fa fa-circle nav-icon"></i>
                        <p>Cargos</p>
                    </router-link>
                </li>
              <li class="nav-item">
                <router-link to="/usuarios" class="nav-link">
                  <i class="fas fa-users nav-icon"></i>
                  <p>Usuários</p>
                </router-link>
              </li>
            </ul>
          </li>
              </p>
            </router-link>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-power-off red"></i>
                <p>{{ __('Logout') }}</p>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        {{-- @yield('content') --}}
        <div>
            <router-view></router-view>
            <vue-progress-bar></vue-progress-bar>
            {{-- <grafica-component></grafica-component> --}}
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->
<script src="/js/app.js"></script>
</body>
</html>
