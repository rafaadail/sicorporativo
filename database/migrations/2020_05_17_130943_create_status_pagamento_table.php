<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusPagamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_pagamento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao');
            $table->timestamps();
            $table->integer('id_usuario_criacao');
            $table->integer('id_usuario_alteracao');
            $table->foreign('id_usuario_criacao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_usuario_alteracao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_pagamento');
    }
}
