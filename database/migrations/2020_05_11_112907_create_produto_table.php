<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo_barras');
            $table->string('nome_produto');
            $table->integer('id_categoria');
            $table->string('fabricante');
            $table->integer('quantidade');
            $table->float('valor_produto');
            $table->string('plataforma')->nullable();
            $table->string('prazo_garantia')->nullable();
            $table->integer('id_usuario_criacao');
            $table->integer('id_usuario_alteracao');
            $table->timestamps();
            $table->foreign('id_categoria')->references('id')->on('categoria')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_usuario_criacao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_usuario_alteracao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto');
    }
}
