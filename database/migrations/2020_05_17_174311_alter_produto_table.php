<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProdutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produto', function (Blueprint $table) {
            $table->dropColumn('fabricante');
            $table->dropColumn('plataforma');
            $table->integer('id_fabricante')->default(1);
            $table->integer('id_plataforma')->default(1);
            $table->boolean('item_raro_colecionador')->default(false);
            $table->foreign('id_fabricante')->references('id')->on('fabricante')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_plataforma')->references('id')->on('plataforma')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produto', function (Blueprint $table) {
            $table->dropColumn('id_fabricante');
            $table->dropColumn('id_plataforma');
            $table->dropColumn('item_raro_colecionador');
            $table->string('fabricante')->nullable();
            $table->string('plataforma')->nullable();
        });
    }
}
