<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormaPagamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forma_pagamento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_pagamento');
            $table->timestamps();
            $table->integer('id_usuario_criacao');
            $table->integer('id_usuario_alteracao');
            $table->foreign('id_usuario_criacao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_usuario_alteracao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forma_pagamento');
    }
}
