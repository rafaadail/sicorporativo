<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cargo')->nullable();
            $table->integer('id_centro_custo')->nullable();
            $table->string('nome')->nullable();
            $table->string('login')->unique();
            $table->string('senha');
            $table->string('email')->unique();
            $table->string('tipo')->default('user');
            $table->tinyInteger('status')->default(1);
            $table->string('imagem')->nullable()->default('profile.png');
            $table->timestamps();
            $table->foreign('id_cargo')->references('cargo')->on('id')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_centro_custo')->references('centro_custo')->on('id')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
