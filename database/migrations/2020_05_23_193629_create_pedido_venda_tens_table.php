<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoVendaTensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_venda_itens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pedido_venda');
            $table->integer('id_produto');
            $table->integer('quantidade');
            $table->decimal('valor_unitario');
            $table->timestamps();
            $table->foreign('id_pedido_venda')->references('id')->on('pedido_venda')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('id_produto')->references('id')->on('produto')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_venda_itens');
    }
}
