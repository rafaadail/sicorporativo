<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoVendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_venda', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cliente');
            $table->integer('id_forma_pagamento');
            $table->integer('id_status_pagamento');
            $table->integer('id_status_venda');
            $table->timestamp('data_venda');
            $table->decimal('valor_venda');
            $table->timestamps();
            $table->integer('id_usuario_criacao');
            $table->integer('id_usuario_alteracao');
            $table->foreign('id_usuario_criacao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_usuario_alteracao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_cliente')->references('id')->on('cliente')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_forma_pagamento')->references('id')->on('forma_pagamento')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_status_pagamento')->references('id')->on('status_pagamento')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_status_venda')->references('id')->on('status_venda')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_venda');
    }
}
