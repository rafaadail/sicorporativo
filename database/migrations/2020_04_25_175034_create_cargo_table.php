<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCargoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao')->unique();
            $table->integer('id_usuario_criacao')->unsigned();
            $table->integer('id_usuario_alteracao')->unsigned();
            $table->foreign('id_usuario_criacao')->references('id')->on('usuario')->onDelete('cascade');
            $table->foreign('id_usuario_alteracao')->references('id')->on('usuario')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargo');
    }
}
