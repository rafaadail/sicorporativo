<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rg');
            $table->string('cpf');
            $table->string('nome');
            $table->string('telefone');
            $table->string('email');
            $table->integer('id_usuario_criacao');
            $table->integer('id_usuario_alteracao');
            $table->timestamps();
            $table->foreign('id_usuario_criacao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_usuario_alteracao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
