<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentroTrabalho extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_trabalho', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_centro_custo');
            $table->string('codigo', 100)->unique();
            $table->string('descricao', 100)->unique();
            $table->tinyInteger('status')->default(1);
            $table->integer('id_usuario_criacao');
            $table->integer('id_usuario_alteracao');
            $table->timestamps();
            $table->foreign('id_centro_custo')->references('id')->on('centro_custo')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_usuario_criacao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
            $table->foreign('id_usuario_alteracao')->references('id')->on('usuario')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centro_trabalho');
    }
}
