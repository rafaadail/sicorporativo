<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentroCusto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centro_custo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo')->unique();
            $table->string('descricao')->unique();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('tipo')->nullable();
            $table->integer('id_usuario_criacao');
            $table->integer('id_usuario_alteracao');
            $table->foreign('id_usuario_criacao')->references('id')->on('usuario')->onDelete('cascade');
            $table->foreign('id_usuario_alteracao')->references('id')->on('usuario')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centro_custo');
    }
}
