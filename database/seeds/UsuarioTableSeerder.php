<?php

use App\Models\Usuario;
use Illuminate\Database\Seeder;

class UsuarioTableSeerder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usuario::create([
            'nome' => 'Administrador',
            'login' => 'Admin',
            'senha' => bcrypt('12345678'),
            'email' => 'admin@admin.com',
            'tipo' => 'admin',
            'status' => 1,
            'imagem' => 'profile.png'
        ]);
    }
}
