<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/usuario', function (Request $request) {
    return $request->user();
});

Route::apiResources(['usuario' => 'API\UsuarioController']);
Route::apiResources(['cargo' => 'API\CargoController']);
Route::apiResources(['centrocusto' => 'API\CentroCustoController']);
Route::apiResources(['centrotrabalho' => 'API\CentroTrabalhoController']);
Route::apiResources(['categoria' => 'API\CategoriaController']);
Route::apiResources(['produto' => 'API\ProdutoController']);
Route::apiResources(['cliente' => 'API\ClienteController']);
Route::apiResources(['endereco' => 'API\EnderecoController']);
Route::apiResources(['fabricante' => 'API\FabricanteController']);
Route::apiResources(['plataforma' => 'API\PlataformaController']);
Route::apiResources(['formapagamento' => 'API\FormaPagamentoController']);
Route::apiResources(['statuspagamento' => 'API\StatusPagamentoController']);
Route::apiResources(['statusvenda' => 'API\StatusVendaController']);
Route::apiResources(['pedidovenda' => 'API\PedidoVendaController']);
Route::get('perfil', 'API\UsuarioController@profile');
Route::put('perfil', 'API\UsuarioController@Updateprofile');
Route::get('codigobarras/{codigobarras}', 'API\ProdutoController@codigoBarras');
Route::get('pdv/{id}', 'API\PedidoVendaController@pdv');
// Route::put('endereco/{endereco}', 'API\EnderecoController@update');
