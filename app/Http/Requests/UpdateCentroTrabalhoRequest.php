<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCentroTrabalhoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_centro_custo' => 'required|numeric',
            'codigo' => 'required|string|max:100|unique:centro_trabalho,codigo,'.$this->get('id'),
            'descricao' => 'required|string|max:100|unique:centro_trabalho,descricao,'.$this->get('id')
        ];
    }
}
