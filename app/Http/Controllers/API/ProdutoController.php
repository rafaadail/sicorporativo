<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Produto::select('id', 'nome_produto as text')->get();
        return Produto::latest()->with(['categoria', 'fabricante', 'plataforma'])->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(in_array($request->id_categoria, [4,5])) {
            $messages = [
                'id_plataforma.required' => 'Obrigatório para Categorias (Acessórios e Jogos).',
                'prazo_garantia.required' => 'Obrigatório para Categorias (Acessórios e Jogos).'
            ];

            $this->validate($request, [
                'codigo_barras' => 'required|numeric|min:13',
                'nome_produto' => 'required|min:5',
                'id_fabricante' => 'required|numeric',
                'id_plataforma' => 'required|numeric',
                'quantidade' => 'required|numeric',
                'valor_produto' => 'required|numeric',
                'prazo_garantia' => 'required|string'
            ], $messages);
        }

        if(!in_array($request->id_categoria, [4,5])) {

            $this->validate($request, [
                'codigo_barras' => 'required|numeric|min:13',
                'nome_produto' => 'required|min:5',
                'id_categoria' => 'required|numeric',
                'id_fabricante' => 'required|numeric',
                'id_plataforma' => 'required|numeric',
                'quantidade' => 'required|numeric',
                'valor_produto' => 'required|numeric',
            ]);
        }

        return Produto::create([
            'codigo_barras' => $request->codigo_barras,
            'nome_produto' => $request->nome_produto,
            'id_categoria' => $request->id_categoria,
            'id_fabricante' => $request->id_fabricante,
            'quantidade'=> $request->quantidade,
            'valor_produto' => $request->valor_produto,
            'id_plataforma' => $request->id_plataforma,
            'prazo_garantia' => $request->prazo_garantia,
            'item_raro_colecionador' => $request->item_raro_colecionador,
            'id_usuario_criacao' => auth()->user()->id,
            'id_usuario_alteracao' => auth()->user()->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produto = Produto::findOrFail($id);

        $request->merge(['id_usuario_alteracao' => auth()->user()->id]);
        $produto->update($request->all());

        return ['message' => 'Produto atualizado.'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Produto::findOrFail($id);
        $produto->delete($id);

        return ['message' => 'Produto excluído.'];
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $codigobarras
     * @return \Illuminate\Http\Response
     */
    public function codigobarras($codigobarras)
    {
        // $produto = Produto::find(2);
        // $data = ['data' => $produto];
        // dd(response()->json($produto));
        // return response()->json($data);
        return Produto::where('codigo_barras', '=', $codigobarras )->get()[0];
        // return response()->json(['data'=> $produto], 200);
    }
}
