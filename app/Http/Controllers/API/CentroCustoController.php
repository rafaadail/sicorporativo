<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUpdateCentroCustoRequest;
use App\Models\CentroCusto;
use Illuminate\Http\Request;

class CentroCustoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CentroCusto::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreUpdateCentroCustoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateCentroCustoRequest $request)
    {
        return CentroCusto::create([
            'codigo' => $request->codigo,
            'descricao' => $request->descricao,
            'tipo' => $request->tipo,
            'id_usuario_criacao' => auth()->user()->id,
            'id_usuario_alteracao' => auth()->user()->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\StoreUpdateCentroCustoRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateCentroCustoRequest $request, $id)
    {
        $centro_custo = CentroCusto::findOrFail($id);

        $request->merge(["id_usuario_alteracao" => auth()->user()->id]);
        $centro_custo->update($request->all());

        return ['message' => 'Atualizado informações do Centro de Custo'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $centro_custo = CentroCusto::findOrfail($id);

        $centro_custo->delete($id);

        return ['message' => 'Centro de Custo excluído'];
    }
}
