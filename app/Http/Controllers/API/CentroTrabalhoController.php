<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCentroTrabalhoRequest;
use App\Http\Requests\UpdateCentroTrabalhoRequest;
use App\Models\CentroTrabalho;

class CentroTrabalhoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CentroTrabalho::latest()->with(['centrocusto'])->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreCentroTrabalhoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCentroTrabalhoRequest $request)
    {
        return CentroTrabalho::create([
            'id_centro_custo' => $request->id_centro_custo,
            'codigo' => $request->codigo,
            'descricao' => $request->descricao,
            'id_usuario_criacao' => auth()->user()->id,
            'id_usuario_alteracao' => auth()->user()->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\UpdateCentroTrabalhoRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCentroTrabalhoRequest $request, $id)
    {
        $centro_trabalho = CentroTrabalho::findOrFail($id);

        $request->merge(['id_usuario_alteracao' => auth()->user()->id]);

        $centro_trabalho->update($request->all());
        return ['message' => 'Centro de Trabalho atualizado.'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $centro_trabalho = CentroTrabalho::findOrFail($id);
        $centro_trabalho->delete();

        return ['message' => 'Centro de Trabalho excluído.'];
    }
}
