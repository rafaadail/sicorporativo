<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Endereco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Cliente::latest()->with('endereco')->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // DB::beginTransaction();
        return Cliente::create([
            'rg' => $request->rg,
            'cpf' => $request->cpf,
            'nome' => $request->nome,
            'telefone' => $request->telefone,
            'email' => $request->email,
            'tipo_logradouro' => $request->tipo_logradouro,
            'logradouro'=> $request->logradouro,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'bairro' => $request->bairro,
            'id_cidade' => $request->cidade,
            'id_usuario_criacao' => auth()->user()->id,
            'id_usuario_alteracao' => auth()->user()->id
        ]);

        // $novoEndereco = Endereco::create([
        //     'id_cliente' => $novoCliente->id,
        //     'logradouro'=> $request->logradouro,
        //     'numero' => $request->numero,
        //     'complemento' => $request->complemento,
        //     'bairro' => $request->bairro,
        //     'cidade' => $request->cidade,
        //     'estado' => $request->estado,
        //     'id_usuario_criacao' => auth()->user()->id,
        //     'id_usuario_alteracao' => auth()->user()->id
        // ]);

        // if( $novoCliente && $novoEndereco ) {
        //     DB::commit();
        // }

        // if( !$novoCliente || !$novoEndereco ) {
        //     DB::rollBack();
        // }

        // return Cliente::findOrFail($novoCliente->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::findOrFail($id);

        $request->merge(['id_usuario_alteracao' => auth()->user()->id]);
        $cliente->update($request->all());

        return ['message' => 'Cliente atualizado.'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        $cliente->delete($id);

        return ['message' => 'Cliente excluído.'];
    }
}
