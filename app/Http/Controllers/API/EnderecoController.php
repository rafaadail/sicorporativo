<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Endereco;
use Illuminate\Http\Request;

class EnderecoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Endereco::latest()->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id_cliente' => 'required|numeric',
            'estado' => 'required|string',
            'cidade' => 'required|string',
            'bairro' => 'required|string',
            'numero' => 'required|numeric',
            'logradouro' => 'required|string'
        ]);

        return Endereco::create([
            'logradouro'=> $request->logradouro,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'bairro' => $request->bairro,
            'cidade' => $request->cidade,
            'id_cliente' => $request->id_cliente,
            'id_usuario_criacao' => auth()->user()->id,
            'id_usuario_alteracao' => auth()->user()->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id_cliente' => 'required|numeric',
            'estado' => 'required|string',
            'cidade' => 'required|string',
            'bairro' => 'required|string',
            'numero' => 'required|numeric',
            'logradouro' => 'required|string'
        ]);

        $endereco = Endereco::findOrFail($id);

        $request->merge(['id_usuario_alteracao' => auth()->user()->id]);
        $endereco->update($request->all());

        return ['message' => 'Endereço atualizado.'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $endereco = Endereco::findOrFail($id);
        $endereco->delete($id);

        return ['message' => 'Endereço Excluído.'];
    }
}
