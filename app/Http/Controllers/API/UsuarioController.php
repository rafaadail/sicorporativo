<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUsuarioRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Usuario;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Usuario::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreUsuarioRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsuarioRequest $request)
    {

        return Usuario::create([
            'nome' => $request->nome,
            'login' => $request->login,
            'email' => $request->email,
            'senha' => Hash::make($request->senha),
            'tipo' => $request->tipo,
            'imagem' => $request->imagem,

        ]);
    }

    public function updateProfile(Request $request)
    {
        $usuario = auth('api')->user();

        $this->validate($request, [
            'nome' => 'required|string|max:255',
            // 'login' => 'required|string|max:15|unique:usuario',
            'email' => 'required|string|email|max:255|unique:usuario,email,'.$usuario->id,
            'senha' => 'sometimes|required|min:8'
        ]);


        if($request->imagem != $usuario->imagem){
            $nome = time().'.'. explode('/', explode(':', substr($request->imagem, 0, strpos($request->imagem, ';')))[1])[1];

            \Image::make($request->imagem)->save(public_path('img/perfil/').$nome);
            $request->merge(["imagem" => $nome]);

            $imagemUsuario = public_path('img/perfil/'.$usuario->imagem);
            if(file_exists($imagemUsuario)){
                @unlink($imagemUsuario);
            }
        }

        if(!empty($request->senha)){
            $request->merge(["senha"=> Hash::make($request->senha)]);
        }

        $usuario->update($request->all());
        return ["message"=> "Success"];
    }

    public function profile()
    {
        return auth('api')->user();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = Usuario::findOrFail($id);

        $this->validate($request, [
            'nome' => 'required|string|max:255',
            'login' => 'required|string|max:15|unique:usuario,login,'.$usuario->id,
            'email' => 'required|string|email|max:255|unique:usuario,email,'.$usuario->id,
            'senha' => 'sometimes|min:8'
        ]);

        $usuario->update($request->all());
        return ['message' => 'Atualizado informações do usuário'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::findOrFail($id);

        // // delete the user
        $usuario->delete();

        return ['message' => 'Usuário Excluído'];
    }
}
