<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';

    protected $fillable = [
        'rg', 'cpf', 'nome', 'telefone', 'email', 'id_usuario_criacao', 'id_usuario_alteracao',
        'tipo_logradouro', 'logradouro', 'numero', 'complemento', 'bairro', 'id_cidade'
    ];

    public function usuariocriacao()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario_criacao');
    }

    public function usuarioalteracao()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario_alteracao');
    }

    public function endereco()
    {
        return $this->hasOne(Endereco::class, 'id_cliente', 'id');
    }
}
