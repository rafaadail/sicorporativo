<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produto';

    protected $fillable = [
        'codigo_barras',
        'nome_produto',
        'id_categoria',
        'id_fabricante',
        'quantidade',
        'valor_produto',
        'id_plataform',
        'prazo_garantia',
        'item_raro_colecionador',
        'id_usuario_criacao',
        'id_usuario_alteracao',
    ];

    public function usuariocriacao()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario_criacao');
    }

    public function usuarioalteracao()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario_alteracao');
    }

    public function categoria()
    {
        return $this->hasOne(Categoria::class, 'id', 'id_categoria');
    }

    public function fabricante()
    {
        return $this->hasOne(Fabricante::class, 'id', 'id_fabricante');
    }

    public function plataforma()
    {
        return $this->hasOne(Plataforma::class, 'id', 'id_plataforma');
    }
}
