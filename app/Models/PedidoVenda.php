<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PedidoVenda extends Model
{
    protected $table = 'pedido_venda';

    protected $fillable = [
        'id_cliente', 'id_forma_pagamento', 'id_status_pagamento',
        'id_status_venda', 'data_venda', 'valor_venda',
        'id_usuario_criacao', 'id_usuario_alteracao'
    ];

    public function itens()
    {
        return $this->hasMany(PedidoVendaItens::class, 'id_pedido_venda', 'id');
    }

    public function cliente()
    {
        return $this->hasOne(Cliente::class, 'id', 'id_cliente');
    }

    public function formapagamento()
    {
        return $this->hasOne(FormaPagamento::class, 'id', 'id_forma_pagamento');
    }

    public function statuspagamento()
    {
        return $this->hasOne(StatusPagamento::class, 'id', 'id_status_pagamento');
    }

    public function statusvenda()
    {
        return $this->hasOne(StatusVenda::class, 'id', 'id_status_venda');
    }
}
