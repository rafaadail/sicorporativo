<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CentroTrabalho extends Model
{
    protected $table = 'centro_trabalho';

    protected $fillable = [
        'id_centro_custo', 'codigo', 'descricao', 'status', 'id_usuario_criacao', 'id_usuario_alteracao'
    ];

    public function usuariocriacao()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario_criacao');
    }

    public function usuarioalteracao()
    {
        return $this->hasOne(Usuario::class, 'id', 'id_usuario_alteracao');
    }

    public function centrocusto()
    {
        return $this->hasOne(CentroCusto::class, 'id', 'id_centro_custo');
    }
}
