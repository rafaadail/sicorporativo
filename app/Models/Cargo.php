<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = 'cargo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
       'id', 'descricao', 'id_usuario_criacao', 'id_usuario_alteracao'
   ];

   public function usuariocriacao()
   {
       return $this->belongsTo(Usuario::class, 'cargo', 'id', 'id_usuario_criacao');
   }

   public function usuarioalteracao()
   {
       return $this->belongsTo(Usuario::class, 'cargo', 'id', 'id_usuario_alteracao');
   }
}
