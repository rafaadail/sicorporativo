<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plataforma extends Model
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    protected $table = 'plataforma';

    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $fillable = [
        'id', 'descricao', 'id_usuario_criacao', 'id_usuario_alteracao'
    ];
}
