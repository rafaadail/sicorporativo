<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CentroCusto extends Model
{

    protected $table = 'centro_custo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'codigo', 'descricao', 'tipo', 'id_usuario_criacao', 'id_usuario_alteracao'
    ];

    public function usuarioCriacao()
    {
        return $this->belongsTo(Usuario::class, 'centro_custo', 'id', 'id_usuario_criacao');
    }

    public function usuarioalteracao()
    {
        return $this->belongsTo(Usuario::class, 'centro_custo', 'id', 'id_usuario_alteracao');
    }

    // public function centrotrabalhos(){
    //     return $this->belongsTo(CentroTrabalho::class, 'centro_trabalho', 'id', 'id_centro_trabalho');
    // }
}
