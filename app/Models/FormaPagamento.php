<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormaPagamento extends Model
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    protected $table = 'forma_pagamento';

    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $fillable = [
        'id', 'tipo_pagamento', 'id_usuario_criacao', 'id_usuario_alteracao'
    ];
}
