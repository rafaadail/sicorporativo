<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PedidoVendaItens extends Model
{
    protected $table = 'pedido_venda_itens';

    protected $fillable = [
        'id_pedido_venda', 'id_produdo', 'quantidade',
        'valor_unitario'
    ];

    public function produto()
    {
        return $this->hasMany(Produto::class, 'id_produto', 'id');
    }
}
