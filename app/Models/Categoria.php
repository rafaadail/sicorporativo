<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categoria';

    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $fillable = [
        'id', 'descricao', 'id_usuario_criacao', 'id_usuario_alteracao'
    ];
}
